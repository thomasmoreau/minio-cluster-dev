#!/bin/bash

MINIO_ROOT_USER="minioadmin"
MINIO_ROOT_PASSWORD="miniopass"

# Start Minio server in the background
minio server /data & sleep 2

# Set alias for the node1
mc alias set node1 http://node1:9000 "$MINIO_ROOT_USER" "$MINIO_ROOT_PASSWORD" || exit 1
mc alias set master http://master:9000 "$MINIO_ROOT_USER" "$MINIO_ROOT_PASSWORD" || exit 1

mc mb node1/shared
mc mb master/master-bucket

mc version enable node1/shared
mc version enable master/master-bucket

# Set replication policy
mc replicate add node1/shared-bucket --remote-bucket http://"$MINIO_ROOT_USER":$MINIO_ROOT_PASSWORD@master:9000/master-bucket